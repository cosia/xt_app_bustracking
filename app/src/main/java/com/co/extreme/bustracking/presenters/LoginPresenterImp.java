package com.co.extreme.bustracking.presenters;

import com.co.extreme.bustracking.interactors.LoginInteractorImp;
import com.co.extreme.bustracking.interfaces.views.login.LoginInteractor;
import com.co.extreme.bustracking.interfaces.views.login.LoginPresenter;
import com.co.extreme.bustracking.interfaces.views.login.LoginView;
import com.co.extreme.bustracking.interfaces.views.login.OnLoginFinishListener;

/**
 * Created by Cristian Z. Osia on 12/03/2017.
 */

public class LoginPresenterImp implements LoginPresenter, OnLoginFinishListener {

    private LoginView loginView;
    private LoginInteractor loginInteractor;

    public LoginPresenterImp(LoginView loginView) {
        this.loginView = loginView;
        loginInteractor = new LoginInteractorImp();
    }

    @Override
    public void validarLogin(String user, String pass) {
        if (loginView != null) {
            loginInteractor.validarLogin(user, pass, this);
        }
    }

    @Override
    public void userError() {
        if (loginView != null) {
            loginView.setErrorUser();
        }
    }

    @Override
    public void passError() {
        if (loginView != null) {
            loginView.setErrorPass();
        }
    }

    @Override
    public void exitOperation() {
        if (loginView != null) {
            loginView.goToPrincipal();
        }
    }
}
