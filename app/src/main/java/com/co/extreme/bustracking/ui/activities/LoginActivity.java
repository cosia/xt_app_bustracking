package com.co.extreme.bustracking.ui.activities;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;

import com.co.extreme.bustracking.PrincipalActivity;
import com.co.extreme.bustracking.R;
import com.co.extreme.bustracking.databinding.ActivityLoginBinding;
import com.co.extreme.bustracking.interfaces.views.login.LoginPresenter;
import com.co.extreme.bustracking.interfaces.views.login.LoginView;
import com.co.extreme.bustracking.interfaces.views.login.OnLoginFinishListener;
import com.co.extreme.bustracking.presenters.LoginPresenterImp;

import java.security.Principal;

public class LoginActivity extends AppCompatActivity implements LoginView {

    private ActivityLoginBinding activityLoginBinding;
    private LoginPresenter loginPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        activityLoginBinding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        setSupportActionBar(activityLoginBinding.toolbar);

        loginPresenter = new LoginPresenterImp(this);
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void setErrorUser() {
        activityLoginBinding.contentLayout.editText.setError("Ingrese un Ususario");
    }

    @Override
    public void setErrorPass() {
        activityLoginBinding.contentLayout.editText2.setError("Ingrese una Contraseña");
    }

    @Override
    public void goToPrincipal() {
        startActivity(new Intent(this, PrincipalActivity.class));
    }

    public void validacion(View view) {
        loginPresenter.validarLogin(activityLoginBinding.contentLayout.editText.getText().toString(), activityLoginBinding.contentLayout.editText2.getText().toString());
    }



}
