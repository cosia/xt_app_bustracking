package com.co.extreme.bustracking.interactors;

import com.co.extreme.bustracking.interfaces.views.login.LoginInteractor;
import com.co.extreme.bustracking.interfaces.views.login.OnLoginFinishListener;

/**
 * Created by Cristian Z. Osia on 12/03/2017.
 */

public class LoginInteractorImp implements LoginInteractor {

    @Override
    public void validarLogin(String user, String pass, OnLoginFinishListener onLoginFinishListener) {
        if (!user.equalsIgnoreCase("") && !pass.equalsIgnoreCase("")) {
            onLoginFinishListener.exitOperation();
        } else {
            if (user.equalsIgnoreCase("")) {
                onLoginFinishListener.userError();
            }
            if (pass.equalsIgnoreCase("")) {
                onLoginFinishListener.passError();
            }
        }
    }
}
