package com.co.extreme.bustracking.interfaces.views.login;

/**
 * Created by Cristian Z. Osia on 12/03/2017.
 */

public interface LoginInteractor {

    void validarLogin(String user, String pass, OnLoginFinishListener onLoginFinishListener);
}
