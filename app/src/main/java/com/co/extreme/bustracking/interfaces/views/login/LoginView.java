package com.co.extreme.bustracking.interfaces.views.login;

/**
 * Created by Cristian Z. Osia on 12/03/2017.
 */

public interface LoginView {

    void showProgress();
    void hideProgress();

    void setErrorUser();
    void setErrorPass();

    void goToPrincipal();
}
